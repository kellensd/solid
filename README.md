# Projeto modelo que contém os 5 princípios do SOLID implementado
    
**1. S — Single Responsiblity Principle**
- Princípio da Responsabilidade Única — Uma classe deve ter um, e somente um, motivo para mudar.

**2. O — Open-Closed Principle**
- Princípio Aberto-Fechado — Objetos ou entidades devem estar abertos para extensão, mas fechados para modificação

**3. L — Liskov Substitution Principle**
- Princípio da substituição de Liskov — Uma classe derivada deve ser substituível por sua classe base.

**4. I — Interface Segregation Principle**
- Princípio da Segregação da Interface — Uma classe não deve ser forçada a implementar interfaces e métodos que não irão utilizar.

**5. D — Dependency Inversion Principle**
- Princípio da Inversão de Dependência — Dependa de abstrações e não de implementações.

