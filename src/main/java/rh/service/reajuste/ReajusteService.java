package rh.service.reajuste;

import rh.model.Funcionario;
import java.math.BigDecimal;
import java.util.List;

public class ReajusteService {

    private List<ValidacaoReajuste> validacaoReajustes;

    public ReajusteService(List<ValidacaoReajuste> validacaoReajustes) {
        this.validacaoReajustes = validacaoReajustes;
    }

    public void reajustarSalarioDoFuncionario(Funcionario funcionario, BigDecimal aumento){
        this.validacaoReajustes.forEach(validacaoReajuste -> validacaoReajuste.validar(funcionario, aumento));

        BigDecimal salarioReajustado = funcionario.getSalario().add(aumento);
        funcionario.atualizarSalario(salarioReajustado);
    }
}
